﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mysku_csv_search_parser
{
	class Program
	{
		static void Main(string[] args)
		{


			var request_search = "18650";


			var pages = 1;
			bool pages_ = true;
			using (StreamWriter writer = new StreamWriter(request_search + ".csv", false, new UTF8Encoding(true)))
			{
				writer.WriteLine("title,price,is,read,comment,nrav,plan,link");
				for (int i = 1; i <= pages; i++)
				{
					string url = "https://mysku.ru/search/topics/page" + i + "?q=" + request_search;
					var Webget = new HtmlWeb();
					var doc = Webget.Load(url);
					var v = doc.DocumentNode.SelectNodes("//div[@class='topic']");//.Where(d => d.HasClass("\"topic\""));
					if (pages_)
					{
						pages_ = false;
						var va = doc.DocumentNode.SelectNodes("//*[contains(text(),'последняя')]").First().Attributes["href"].Value;
						int pFrom = va.IndexOf("page") + "page".Length;
						int pTo = va.LastIndexOf("?q=");
						pages = Math.Min(Int32.Parse(va.Substring(pFrom, pTo - pFrom)),100);//101+ not work
					}

					v.ToList().ForEach(x =>
						{
							string is1 = x.InnerHtml.Contains("Пункт №18") ? "+" : "";
							string title = dostring(x.SelectNodes("div[@class='topic-title']").First().InnerText);
							string plan = (x.SelectNodes(".//span[@class='number']") == null) ? "-" : x.SelectNodes(".//span[@class='number']").First().InnerText.Replace("+", "");
							string nrav = dostring(x.SelectNodes(".//span[contains(@class,'number total')]").First().LastChild.InnerText).Replace("+", "");
							string read = x.SelectNodes(".//li[@class='read']").First().InnerText.Split(' ').Last();
							string price = (x.SelectNodes(".//li[@class='price']") == null) ? "-" : x.SelectNodes(".//li[@class='price']").First().InnerText.Replace("Цена: ", "");
							string comment = x.SelectNodes(".//span[@class='red']").First().LastChild.InnerText;
							string link = x.SelectNodes("div[@class='topic-title']/a").First().Attributes["href"].Value;
							writer.WriteLine(string.Join(",", new string[] { title, price, is1, read, comment, nrav, plan, link }.Select(xx => xx.Replace(",", " ").Replace(";", " "))));
						});
				}
			}
		}

		static string dostring(string s)
		{
			return string.Join(" ", s.Replace("\n", "").Replace("\t", "").Replace("\r", "").Replace("&quot;", "\"").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
		}
	}
}
